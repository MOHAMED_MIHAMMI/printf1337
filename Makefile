# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/12/10 14:05:09 by momihamm          #+#    #+#              #
#    Updated: 2023/01/08 21:27:59 by momihamm         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

CFLAGS = -Wall -Werror -Wextra

FILES = ft_address.c \
	ft_putchar.c \
	ft_putnbr.c \
	ft_putnbrbase.c \
	ft_putstr.c \
	ft_printf.c \
	ft_putunsignednbr.c \
	ft_2.c
	
OBJ = $(FILES:.c=.o)

all : $(NAME)

$(NAME) : $(OBJ)
	ar rc $(NAME) $(OBJ)

%.o : %.c ftprintf.h
	$(CC) $(CFLAGS) -c $<

clean :
	rm -f $(OBJ)

fclean : clean
	rm -f $(NAME)

re : fclean all




