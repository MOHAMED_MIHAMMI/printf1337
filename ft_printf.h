/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/10 14:29:10 by momihamm          #+#    #+#             */
/*   Updated: 2023/01/04 12:36:19 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <unistd.h>
# include <stdarg.h>

void	ft_putchar(char c, int *len);
void	ft_putnbr(int n, int *len);
void	ft_putnbrbase(unsigned int nbr, char forma, int *len);
void	ft_putstr(char *s, int *len);
int		ft_printf(char *format, ...);
void	ft_address(unsigned long long ptr, int *len);
void	ft_putunsignednbr(unsigned int n, int *len);
void	ft_2(char character, int *ptr, va_list *ptr_of_format);

#endif
