/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/10 13:58:29 by momihamm          #+#    #+#             */
/*   Updated: 2023/01/02 20:55:31 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_putstr(char *s, int *len)
{
	int	indx;

	indx = 0;
	if (s == NULL)
	{
		ft_putstr("(null)", len);
		return ;
	}
	while (s[indx])
	{
		ft_putchar(s[indx], len);
		indx++;
	}
}
