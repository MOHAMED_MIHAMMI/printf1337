/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_address.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/16 13:34:40 by momihamm          #+#    #+#             */
/*   Updated: 2023/01/02 20:55:04 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_putadd(unsigned long long nbr, char forma, int *len)
{
	if (nbr >= 10 && nbr <= 15)
	{
		if (forma == 'x')
			ft_putchar (nbr + 87, len);
		else
			ft_putchar(nbr + 55, len);
	}
	else if (nbr >= 0 && nbr < 10)
		ft_putchar(nbr + '0', len);
	else if (nbr >= 16)
	{
		ft_putadd(nbr / 16, forma, len);
		ft_putadd(nbr % 16, forma, len);
	}
}

void	ft_address(unsigned long long ptr, int *len)
{
	ft_putstr("0x", len);
	ft_putadd (ptr, 'x', len);
}

// int main()
// {
// 	char o[]= "PNL";
// 	printf ("%p\n",o);
// 	ft_address(o);
// }
