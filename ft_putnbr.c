/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/10 13:58:12 by momihamm          #+#    #+#             */
/*   Updated: 2023/01/02 20:55:23 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_putnbr(int n, int *len)
{
	if (n >= 0 && n < 10)
		ft_putchar(n + '0', len);
	else if (n >= 10)
	{
		ft_putnbr(n / 10, len);
		ft_putnbr(n % 10, len);
	}
	else if (n < 0 && n > -2147483648)
	{
		n *= -1;
		ft_putchar('-', len);
		ft_putnbr(n, len);
	}
	else
	{
		ft_putstr("-2147483648", len);
	}
}

// #include <stdio.h>

// int main()
// {
// 	ft_putnbr(1000);
// }
