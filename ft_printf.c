/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/19 16:36:46 by momihamm          #+#    #+#             */
/*   Updated: 2023/01/08 21:23:17 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_printf(char *format, ...)
{
	int		len;
	int		i;
	va_list	list_of_argument;

	len = 0;
	i = 0;
	va_start (list_of_argument, format);
	while (format[i])
	{
		if (format[i] == '%')
		{
			i++;
			if (format[i] == '\0')
				break ;
			ft_2(format[i], &len, &list_of_argument);
		}
		else
			ft_putchar(format[i], &len);
		i++;
	}
	va_end(list_of_argument);
	return (len);
}
