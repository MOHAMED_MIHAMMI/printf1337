/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putunsignednbr.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/23 12:30:32 by momihamm          #+#    #+#             */
/*   Updated: 2023/01/02 20:55:35 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_putunsignednbr(unsigned int n, int *len)
{
	if (n >= 0 && n < 10)
		ft_putchar (n + '0', len);
	else if (n >= 10)
	{
		ft_putunsignednbr (n / 10, len);
		ft_putunsignednbr (n % 10, len);
	}
}
