/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_2.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/02 20:37:24 by momihamm          #+#    #+#             */
/*   Updated: 2023/01/08 20:40:06 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_2(char character, int *ptr, va_list *ptr_of_format)
{
	if (character == 'c')
		ft_putchar(va_arg (*ptr_of_format, int), ptr);
	else if (character == 's')
		ft_putstr(va_arg(*ptr_of_format, char *), ptr);
	else if (character == 'p')
		ft_address(va_arg (*ptr_of_format, unsigned long long), ptr);
	else if (character == 'd')
		ft_putnbr(va_arg (*ptr_of_format, int), ptr);
	else if (character == 'i')
		ft_putnbr(va_arg (*ptr_of_format, int), ptr);
	else if (character == 'u')
		ft_putunsignednbr (va_arg (*ptr_of_format, unsigned int), ptr);
	else if (character == 'x')
		ft_putnbrbase (va_arg (*ptr_of_format, unsigned int), 'x', ptr);
	else if (character == 'X')
		ft_putnbrbase (va_arg (*ptr_of_format, unsigned int), 'X', ptr);
	else if (character == '%')
		ft_putchar(37, ptr);
	else
		ft_putchar(character, ptr);
}
