/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbrbase.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/10 13:58:46 by momihamm          #+#    #+#             */
/*   Updated: 2023/01/02 20:55:27 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_putnbrbase(unsigned int nbr, char forma, int *len)
{
	if (nbr >= 10 && nbr <= 15)
	{
		if (forma == 'x')
			ft_putchar (nbr + 87, len);
		else
			ft_putchar(nbr + 55, len);
	}
	else if (nbr >= 0 && nbr < 10)
		ft_putchar(nbr + '0', len);
	else if (nbr >= 16)
	{
		ft_putnbrbase(nbr / 16, forma, len);
		ft_putnbrbase(nbr % 16, forma, len);
	}
}
